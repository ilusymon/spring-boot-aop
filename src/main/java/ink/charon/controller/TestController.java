package ink.charon.controller;

import ink.charon.aop.TestAnnotation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @TestAnnotation
    @GetMapping("/test")
    public String test() {
        System.out.println("test running");
        return "okk";
    }
}
